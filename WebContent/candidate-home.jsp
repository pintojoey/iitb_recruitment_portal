<!DOCTYPE html>
<%@page import="internships.TechnicalApplication"%>
<html>
<head>
<%@page import="database.Query"%>
<%@page import="internships.Internship"%>
<%@page import="java.util.ArrayList"%>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Candidate | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  
  <![endif]-->
  <script>
  function detail(id){
	  window.location="internship-detail?index="+id;
  }
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <jsp:include page="candidate-sidebar.jsp"></jsp:include>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Candidate
        <small>Home</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <!-- Main row -->
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Internship Status</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover table-bordered" id="internshiplist">
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Category</th>
                  <th>Opening Date</th>
                  <th>Closing Date</th>
                  <th>Duration</th>
                  <th>Status</th>
                  <th>My Application</th>
                  <th>Applicants</th>
                </tr>
                <%
               ArrayList<Internship> internship_list=Query.getInterships();
                java.util.Iterator<Internship> internship_iterator = internship_list.iterator();
                Internship current;
                while (internship_iterator.hasNext()) {
                	 current=internship_iterator.next();
        			out.print("<tr onclick='detail("+current.getId()+")'style='cursor: pointer;' > ");
        			out.print("<td>"+current.getId()+"</td>");
        			out.print("<td >"+current.getTitle()+"</td>");
        			 out.print("<td>"+current.getCategory()+"</td>");
        			out.print("<td>"+current.getOpening()+"</td>");
        			out.print("<td>"+current.getClosing()+"</td>");
        			out.print("<td>"+current.getDuration()+" month(s) </td>");
        			
        			String status=current.getStatus();
        			
        			if(status.equals("open")){
        				out.print("<td><span class='label label-success'>open</span></td>");
        			
        			}
        			else if(status.equals("closed")){
        				out.print("<td><span class='label label-danger'>closed</span></td>");
        			
        			}
        			else if(status.equals("not open")){
        				out.print("<td><span class='label label-primary'>not open</span></td>");
        			
        			}
        			else if(status.equals("closing soon")){
        				out.print("<td><span class='label label-warning'>closing soon</span></td>");
        			
        			}
        			
        			if(current.getCategory().equals("Technical")){
        				TechnicalApplication tech_application=Query.getTechnicalApplication(session.getAttribute("username").toString(), current.getId());
        				
        				if(tech_application==null){
        					
        					out.print("<td><span class='label label-success'>Not Applied</span></td>");
        				}
        					else
        				{
		        				switch(tech_application.getStatus()){
		        				case 1:
		        					out.print("<td><span class='label label-warning'>Applied</span></td>");
		        				break;
		        				case 3:
		        					out.print("<td><span class='label label-success'>Shortlisted for Round 2</span></td>");
		        				break;
		        				case 2:
		        					out.print("<td><span class='label label-danger'>Waitlisted in Round 1</span></td>");
		        				break;
		        				case 4:
		        					out.print("<td><span class='label label-danger'>Waitlisted in Round 2</span></td>");
		        				break;
		        				case 5:
		        					out.print("<td><span class='label label-success'>Shortlisted for Round 3</span></td>");
		        				break;
		        				case 6:
		        					out.print("<td><span class='label label-danger'>Waitlisted in Round 3</span></td>");
		        				break;
		        				case 7:
		        					out.print("<td><span class='label label-warning'>Shortlisted</span></td>");
		        				break;
		        				
		        					default:
		        						out.print("<td><span class='label label-warning'>ERROR</span></td>");
		        						break;
		        				}
		        				
        				}
        				
        			}
        			else{
        				out.print("<td>Unavailable</td>");
        			}
        			
        			out.print("<td>"+current.getApplicants()+"</td>");
        			
        			out.print("</tr>");
        			
        		}
                %>
                  
                               </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
     
    </section>
    <!-- /.content -->
  </div>
 </div>
  <!-- /.content-wrapper -->
  <jsp:include page="candidate-footer.jsp" />
<!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

</body>
</html>
