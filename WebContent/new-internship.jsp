<!DOCTYPE html>
<%@page import="database.Query"%>
<%@page import="internships.Internship"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page import ="java.sql.*" %>
<%@ page import ="useraccounts.Recruiter_Profile.*" %>
<%@ page import="java.util.ArrayList.*" %>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Recruiter | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->


</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <jsp:include page="recruiter-sidebar.jsp"></jsp:include>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      
    <section class="content col-lg-6">
      
          <div class="box box-warning ">
              
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form">
                <!-- text input -->
                <div class="form-group">
                  <label>Title</label>
                  <input type="text" id="title" class="form-control" onkeyup="update()" onclick="closeall()" placeholder="Title" 
                  value=""/>
                </div>
                
                <div class="form-group">
                  <label>Category</label>
                  <select class="form-control" onchange="update()" id="category" onclick="closeall()" >
                  
                  	 <option value="" selected="true" disabled="disabled">Select your option</option>
                    <option value="Technical">Technical</option>
                    <option value="Non-Technical">Non-Technical</option>
                    
                  </select>
                </div>
                <label>Application Deadlines</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" onchange="update()" class="form-control pull-right" id="range" value="">
                </div>
                <div class="col-lg-6" style="margin-top:20px;margin-bottom:20px;">
               <label>Duration in months</label>
                <div class="input-group">
                 
                 <input id="duration" type="number" min="1" max="10" class="knob" value="1" data-width="90" data-height="90" data-fgColor="blue">
                
                  
                </div>
                </div>
                <div class="col-lg-6" style="margin-top:20px;margin-bottom:20px;">
               <label>Seats</label>
                <div class="input-group">
                 
                 <input id="seats" type="number" min="1" max="10" class="knob" value="1" data-width="90" data-height="90" data-fgColor="green">
                
                  
                </div>
                </div>
                <div class="form-group">
                 <label  style="cursor: pointer;" onclick="ckedit('abouteditor')" >Internship Description</label> 
                    <div  style="cursor: pointer;border-style:solid;border-width:1px" id="abouteditor" name="abouteditor" onclick="ckedit('abouteditor')" 
                    ></div>
              </div>
             
<!-- Select multiple-->
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          

    </section>
    
    <section class="content col-lg-6">
      
          <div class="box box-success ">
              
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form">
                <!-- text input -->
                
                 <div class="form-group">
                  <label style="cursor: pointer;" onclick="ckedit('eligibilityeditor')">Who can apply</label>
                      <div  style="cursor: pointer;border-style:solid;border-width:1px"  id="eligibilityeditor" name="eligibilityeditor" onclick="ckedit('eligibilityeditor')" 
                      ></div>
              </div>
              <div class="form-group">
                  <label style="cursor: pointer;" onclick="ckedit('informationeditor')">Additional Information</label>
                      <div  style="cursor: pointer;border-style:solid;border-width:1px"  id="informationeditor" name="informationeditor" onclick="ckedit('informationeditor')" 
                      ></div>
              </div>
                <div class="alert alert-danger alert-dismissible" style="display:none" id="error">
                <i class="icon fa fa-ban"></i> 
              <span id="message"></span> 
              </div>
                <div>
                <button type="button" id="save" class="btn btn-block btn-success"  style="display:none" onclick="savechanges()" >Post Internship</button>
                </div>
				
                <!-- textarea -->



					</form>
            </div>
            <!-- /.box-body -->
          </div>
          

    </section>
    
    <!-- /.content -->
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
   </div>
  <jsp:include page="recruiter-footer.jsp"></jsp:include>
    
<!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script defer>

var title,category,range,duration,seats,about,eligibility,information;
update();
document.getElementById("save").style.display="none"; //hiding save button
duration=document.getElementById('duration').value;
seats=document.getElementById('seats').value;
about=document.getElementById('abouteditor').innerHTML;
eligibility=document.getElementById('eligibilityeditor').innerHTML;
information=document.getElementById('informationeditor').innerHTML;


  function savechanges(){
	  update();
	 closeall();
 	  if(validate()){
			
			 var xmlhttp=new XMLHttpRequest();
			    xmlhttp.onreadystatechange=function() {
			        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			      
			if (xmlhttp.responseText.indexOf("Error")!=-1){
				document.getElementById("error").style.display="";
				document.getElementById("message").innerHTML="There was an error saving changes";
			 
			}
			else {
				window.location="edit-internship?index="+xmlhttp.responseText;
			}


					}
					}
			    xmlhttp.open("POST","NewInternship",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			    xmlhttp.send("about="+about+"&category="+category+"&close="+close+"&duration="+duration+"&eligibility="+eligibility+"&information="+information+
			    "&open="+open+"&seats="+seats+"&title="+ title);
			    
			    document.getElementById('save').style.display="";
			document.getElementById('error').style.display="none";
		  }
  }
  function update(){
	  title=document.getElementById('title').value;
	  category=document.getElementById('category').value;
	  range=document.getElementById('range').value;
	  try{
		  open=range.substring(0,range.indexOf(" "));
		  close=range.substring(range.indexOf("-")+2,range.length);
		  
	  }
	  catch (Exception){
		  open="";
		  close="";
	  }
	  
	  range="";
      document.getElementById('save').style.display="";
  }
  function validate(){
	  if(title==""||category=="" || open=="" || close=="" ||duration=="" || seats=="" || about=="" || eligibility=="" || information=="")
	  {
		  document.getElementById('error').style.display="";
		  document.getElementById('message').innerHTML=" Please Fill All Fields";
		  return false;}
	  else {
		  
		  document.getElementById('error').style.display="none";
		  return true;}
  }
  function ckedit(box){
	  
	  closeall();
	  
		var data=document.getElementById(box).innerHTML;
		  document.getElementById(box).innerHTML="";
		  CKEDITOR.replace(box);
		  CKEDITOR.instances[box].setData(data);
		  CKEDITOR.instances[box].on('change', function() {
			  if(box=='abouteditor') about=CKEDITOR.instances[box].getData();
			  if(box=='eligibilityeditor') eligibility=CKEDITOR.instances[box].getData();
			  if(box=='informationeditor') information=CKEDITOR.instances[box].getData();
		  update();
		  });
	  
	  }
  function closeall(){
	  for(name in CKEDITOR.instances)
	  {
		  
		  var data=CKEDITOR.instances[name].getData();
		  
	      CKEDITOR.instances[name].destroy(true);
	      document.getElementById(name).innerHTML=data;
	  }
  }

 
  $(function () {
	    /* jQueryKnob */
 		$('#range').daterangepicker();
	    $("#seats").knob({
	     
	       max:10,
	      draw: function () {
	    	  this.max=10;
	    	  
	      },change: function (value) {
	            seats=value.toFixed(0);
	            update();
	        }
	    
	    });
	    $("#duration").knob({
		     
		       max:10,
		      draw: function () {
		    	  this.max=10;
		    	  
		      },change: function (value) {
		            duration=value.toFixed(0);
		            update();
		        }
		    
		    });
	    
	    
	    /* END JQUERY KNOB */
  });
  
 
</script>
<!-- Bootstrap WYSIHTML5 -->

</body>
</html>
