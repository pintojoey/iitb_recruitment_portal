<%@page import="java.util.Iterator"%>
<%@page import="mongo.Notification"%>
<%@page import="java.util.ArrayList"%>
<%@page import="mongo.MongoQuery"%>
<%@page import="java.text.SimpleDateFormat"%>
  <script>
  updateNotifications();
 
	
  function updateNotifications(){
	  var xmlhttp=new XMLHttpRequest();
		
	    xmlhttp.onreadystatechange=function() {
	        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
	var response=JSON.parse(xmlhttp.responseText);
    var count=response.count;
    if(count=="0"){
    document.getElementById('notifications_count').innerHTML="You have no unread notifications";
    document.getElementById('notifications_top').innerHTML="";
    document.getElementById('notifications').innerHTML='<center><i style="font-size:200px;text-align:center" class="fa fa-thumbs-o-up"></i></center>';
    return;
    }
    else if (count=="1")document.getElementById('notifications_count').innerHTML="You have 1 unread notification";
    else document.getElementById('notifications_count').innerHTML="You have "+count+" unread notifications";
	document.getElementById('notifications').innerHTML=response.list;
	document.getElementById('notifications_top').innerHTML=count;
			}
			}
	    xmlhttp.open("POST",'GetNotifications',true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	    xmlhttp.send();
  }
  function markRead(id){
		
		var xmlhttp=new XMLHttpRequest();
		
	    xmlhttp.onreadystatechange=function() {
	        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
	        	updateNotifications();
			}
			}
	    xmlhttp.open("POST","MarkReadNotification",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	    xmlhttp.send("notification="+id);
  }
  window.setInterval(function(){ updateNotifications();}, 5000); 
  </script>
  
  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>IIT</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>IIT</b>BOMBAY</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

<%

  useraccounts.Recruiter_Profile current_user=database.Query.getRecruiterProfile(session.getAttribute("username").toString());
%>		
					
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning" id="notifications_top">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header" id="notifications_count"></li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu" id="notifications">
                
                </ul>
              </li>
              <li class="footer"><a href="notifications-recruiter">View all</a></li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><% 
					String username=current_user.getName();
					out.print(username);
										
					%></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                 <% out.print(username);%> - 			
					<% 	out.print(current_user.getPost());	%>
                  <small>- <% 
                 
                  java.util.Date date =  new SimpleDateFormat("yyyy-M-dd").parse(current_user.getJoining().toString());
                 
                                    
                          out.print(new SimpleDateFormat("MMMM y").format(date).toString()); %></small>
                </p>
              </li>
             
              <!-- Menu Footer-->
              <li class="user-footer">
                
                <div class="pull-right">
                  <a href="SignOut" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><%out.print(username); %></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="recruiter-home">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
          </a>
          
        </li>
        <li >
          <a href="new-internship">
            <i class="fa fa-plus-square"></i> <span>New Internship</span> 
          </a>
          
        </li>
        <li >
          <a href="round1">
            <i class="fa fa-bolt"></i> <span>Round1</span> 
          </a>
          
        </li>
        <li >
          <a href="round2">
            <i class="fa fa-bolt"></i> <span>Round2</span> 
          </a>
          
        </li>
        <li >
          <a href="round3">
            <i class="fa fa-bolt"></i> <span>Round3</span> 
          </a>
          
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

