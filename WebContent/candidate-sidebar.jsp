<%@page import="java.util.Iterator"%>
<%@page import="mongo.Notification"%>
<%@page import="java.util.ArrayList"%>
<%@page import="mongo.MongoQuery"%>
<%@page import="java.text.SimpleDateFormat"%>
  <script>
  updateNotifications();
 
	
  function updateNotifications(){
	  var xmlhttp=new XMLHttpRequest();
		
	    xmlhttp.onreadystatechange=function() {
	        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
	var response=JSON.parse(xmlhttp.responseText);
    var count=response.count;
    if(count=="0"){
    document.getElementById('notifications_count').innerHTML="You have no unread notifications";
    document.getElementById('notifications_top').innerHTML="";
    document.getElementById('notifications').innerHTML='<center><i style="font-size:200px;text-align:center" class="fa fa-thumbs-o-up"></i></center>';
    return;
    }
    else if (count=="1")document.getElementById('notifications_count').innerHTML="You have 1 unread notification";
    else document.getElementById('notifications_count').innerHTML="You have "+count+" unread notifications";
	document.getElementById('notifications').innerHTML=response.list;
	document.getElementById('notifications_top').innerHTML=count;
			}
			}
	    xmlhttp.open("POST",'GetNotifications',true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	    xmlhttp.send();
  }
  function markRead(id){
		
		var xmlhttp=new XMLHttpRequest();
		
	    xmlhttp.onreadystatechange=function() {
	        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
	        	updateNotifications();
			}
			}
	    xmlhttp.open("POST","MarkReadNotification",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	    xmlhttp.send("notification="+id);
  }
  window.setInterval(function(){ updateNotifications();}, 5000); 
  </script>
<%

useraccounts.Candidate_Profile current_user=database.Query.getCandidateProfile(session.getAttribute("username").toString());

%>
<%@page import="java.text.SimpleDateFormat"%>
  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>IIT</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>IIT</b> Bombay</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning" id="notifications_top"></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header" id="notifications_count"></li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu" id="notifications">
                
                </ul>
              </li>
              <li class="footer"><a href="notifications-candidate">View all</a></li>
            </ul>
          </li><!-- Tasks: style can be found in dropdown.less -->
         <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"> <%@ page import ="java.sql.*" %>
										
					<% 
					String username=session.getAttribute("username").toString();
					
					out.print(username);
					%></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                 <% out.print(current_user.getName());%>  			
					<% 	
					if(current_user.getRole().equals("applicant")){}
					else {
					out.print(" - "+current_user.getRole());	%>
					 
                  <small>
                  since <% 
                 
                  java.util.Date date =  new SimpleDateFormat("yyyy-M-dd").parse(current_user.getJoining().toString());
                 
                                    
                          out.print(new SimpleDateFormat("MMMM y").format(date).toString()); %></small>
                         <%} %>
                </p>
              </li>
              <!-- Menu Body -->
             <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="candidate-resume" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="SignOut" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="SignOut" title="Sign Out"><i class="fa fa-sign-out"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><%out.print(current_user.getName()) ;%></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">Candidate Portal</li>
        <li >
          <a href="candidate-home">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
          </a>
          
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>My Account</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="candidate-resume"><i class="fa fa-circle-o"></i>My Resume</a></li>
            
          </ul>
        </li>
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

