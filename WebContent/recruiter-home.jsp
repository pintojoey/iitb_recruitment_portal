<!DOCTYPE html>
<%@ page import="database.Query"%>
<%@ page import="internships.Internship"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import ="java.sql.*" %>
<%@ page import ="useraccounts.Recruiter_Profile.*" %>
<%@ page import="java.util.ArrayList.*" %>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Recruiter | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <!-- Daterange picker -->
  <!-- bootstrap wysihtml5 - text editor -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<script>
  function detail(id){
	  window.location="internship-detail?index="+id;
  }
  function editdetail(id){
	  window.location="edit-internship?index="+id;
  }
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <jsp:include page="recruiter-sidebar.jsp"></jsp:include>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
<%
int stat[]=new int[9];
int total=0;
for(int i=1;i<=7;i++){
	stat[i]=Query.getApplications(i).size();
	total+=stat[i];
}
	%>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><%=total %></h3>

              <p>Candidates Registered</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="recruiter-home.jsp" class="small-box-footer">Refresh<i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><%=stat[7]%></h3>

              <p>Candidates Hired</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="round3.jsp" class="small-box-footer">View List<i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><%=stat[1] %></h3>

              <p>New applications</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="round1.jsp" class="small-box-footer">View List<i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><%="<sub>R1</sub> "+stat[2]+"<sub>R2</sub> "+stat[4]+"<sub>R3</sub> "+stat[6] %></h3>

              <p>Roundwise Waitlisted candidates</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="round1.jsp" class="small-box-footer">View List<i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      
      <div class="row">
        <div class="col-xs-12" >
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Internship Status</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding" >
              <table class="table table-hover table-bordered" id="internshiplist">
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Category</th>
                  <th>Opening Date</th>
                  <th>Closing Date</th>
                  <th>Duration</th>
                  <th>Status</th>
                  
                  <th>Applicants</th>
                  <th>Seats</th>
                  <th>Hired</th>
                  <th>Modify</th>
                </tr>
                <%
               ArrayList<Internship> internship_list=Query.getInterships();
                java.util.Iterator<Internship> internship_iterator = internship_list.iterator();
                Internship current;
                while (internship_iterator.hasNext()) {
                	 current=internship_iterator.next();
                	 out.print("<tr  > ");
        			out.print("<td>"+current.getId()+"</td>");
        			 
        			out.print("<td onclick='editdetail("+current.getId()+")'style='cursor: pointer;'>"+current.getTitle()+"</td>");
        			out.print("<td>"+current.getCategory()+"</td>");
        			out.print("<td>"+current.getOpening()+"</td>");
        			out.print("<td>"+current.getClosing()+"</td>");
        			out.print("<td>"+current.getDuration()+"</td>");
        			String status=current.getStatus();
        			if(status.equals("open")){
        				out.print("<td><span class='label label-success'>open</span></td>");
        			
        			}
        			if(status.equals("closed")){
        				out.print("<td><span class='label label-danger'>closed</span></td>");
        			
        			}
        			if(status.equals("not open")){
        				out.print("<td><span class='label label-primary'>not open</span></td>");
        			
        			}
        			if(status.equals("closing soon")){
        				out.print("<td><span class='label label-warning'>closing soon</span></td>");
        			
        			}
        			
        			
        			out.print("<td>"+current.getApplicants()+"</td>");
        			out.print("<td>"+current.getSeats()+"</td>");
        			out.print("<td>"+current.getHired()+"</td>");
        			out.print("<td style='font-size:30px'><i class='fa fa-edit' style='color:blue;cursor:pointer' onclick='editdetail("+current.getId()+")' ></i> <i class='fa fa-trash-o' style='color:red;cursor:pointer' onclick='deleteInternship("+current.getId()+")'></i></td>");
        			out.print("</tr>");
        			
        		}
                %>
                  
                               </table>
                               
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
   </div>
  <jsp:include page="recruiter-footer.jsp"></jsp:include>
    
<!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<!-- datepicker -->
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
function deleteInternship(internship_id){
	
	if (confirm('Are you sure you want to delete this internship? All relevant student information will be lost.')) {
	    // Save it!
	
	 var xmlhttp=new XMLHttpRequest();
	    xmlhttp.onreadystatechange=function() {
	        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
	
 			location.reload();

			}
			}
	    xmlhttp.open("POST","DeleteInternship",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	    xmlhttp.send('internship_id='+internship_id);
	}
	else 
		return;
}
</script>
 
</body>
</html>
