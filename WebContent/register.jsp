<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>IIT-B | Internships</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script>
  function register(){

	  document.getElementById("error").style.display="none";
		var name=document.getElementById("name").value;
		var email=document.getElementById("email").value;
		var password=document.getElementById("password").value;
		var repeat_password=document.getElementById("repeat-password").value;
		if(name=="" ||email==""||password==""||repeat_password==""){
			document.getElementById("error").style.display="";
			document.getElementById("error_message").innerHTML="Please Fill All Fields";
			
			return;
		}
		
		var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
		if(!email.match(mailformat))  
		{  
			document.getElementById("error").style.display="";
			document.getElementById("error_message").innerHTML="Please Enter Valid Email";  
		
		return ;  
		}
		if(password!=repeat_password)  
		{  
			document.getElementById("error").style.display="";
			document.getElementById("error_message").innerHTML="Passwords do not match";  
			document.getElementById("repeat-password").value="";
			document.getElementById("password").value="";
		
		return ;  
		}
		if(password.length<6)  
		{  
			document.getElementById("error").style.display="";
			document.getElementById("error_message").innerHTML="Password must contain atleast 6 characters";  
			document.getElementById("repeat-password").value="";
			document.getElementById("password").value="";
		
		return ;  
		}
		var type="candidate";
		if(!document.getElementById("iscandidate").checked) type="recruiter";
		
		var xmlhttp=new XMLHttpRequest();
		document.getElementById("register_button").disabled=true;
	    xmlhttp.onreadystatechange=function() {
	        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
	        	
	if (xmlhttp.responseText=="Exists"){
		document.getElementById("error").style.display="";
		document.getElementById("error_message").innerHTML="Email Already Exists";
	}
	if (xmlhttp.responseText=="Login"){
		document.getElementById("success").style.display="";
		if(type=="candidate"){
			
			document.getElementById("success_message").innerHTML="Please Check Your Email for Verification Link";
		}
		if(type=="recruiter"){
			
			document.getElementById("success_message").innerHTML="Please Wait For Admin Approval";
		}
	}
	document.getElementById("register_button").style.display("none");
			}
			}
	    xmlhttp.open("POST","SignUp",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	    xmlhttp.send("name="+name+"&email="+email+"&password="+password+"&type="+type);
	}
  </script>
</head>
<body class="hold-transition register-page" style="background-image: url('dist/img/background.jpg');background-size: cover;">
<div class="register-box">
  <div class="register-logo">
    <a href="index2.html"><b>IIT</b>BOMBAY</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <form >
      <div class="form-group has-feedback">
        <input type="text" class="form-control" id="name" placeholder="Full name">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" name="email" id="email"class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" id="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" id="repeat-password"placeholder="Retype password">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div id="error" style="display:none">
       <div class="alert alert-danger alert-dismissible" >
                
            <span id="error_message"></span>    
              </div>
     </div>
     <div id="success" style="display:none">
       <div class="alert alert-success alert-dismissible" >
                
            <span id="success_message"></span>    
              </div>
     </div>
              <!-- radio -->
              <div class="form-group">
                <label>
                  <input type="radio" name="r3" class="flat-red" id="iscandidate" checked>
                  Candidate
                </label>
                <label>
                  <input type="radio" name="r3" id="isrecruiter" class="flat-red">
                  Recruiter
                </label>
                
              </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input name="type" type="checkbox"> I agree to the <a href="#">terms</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="button" id="register_button" onclick="register()"  style="display:visible" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    
    <a href="login.jsp" class="text-center">I already have a membership</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
