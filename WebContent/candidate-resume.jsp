<!DOCTYPE html>
<%@page import="database.Query"%>
<%@page import="internships.Internship"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%

useraccounts.Candidate_Profile current_user=database.Query.getCandidateProfile(session.getAttribute("username").toString());
useraccounts.Candidate_Resume user_profile=database.Query.getCandidateResume(session.getAttribute("username").toString());

%>	
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Candidate | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script>
  var username="<%session.getAttribute("username");%>";
  function update(){
	f_name=document.getElementById("f_name").value; 
	l_name=document.getElementById("l_name").value;
	contact=document.getElementById("contact").value;
	interests=document.getElementById("interests").value;
	locat=document.getElementById("location").value;
	summary=document.getElementById("summary").value;
	course=document.getElementById("course").value;
	courseyear=document.getElementById("courseyear").value;
	stream=document.getElementById("stream").value;
	college=document.getElementById("college").value;
	grade=document.getElementById("grade").value;
	document.getElementById("save").style.display="";
	  }
  
  function validate(){
	  
	 if(f_name=="" || l_name=="" || contact==""  || course=="" || courseyear==""|| stream==""|| college=="" || grade=="")
		  
	  {
		  document.getElementById("error").style.display="";
		  document.getElementById("message").innerHTML="Please Fill all fields";
		   
		  return false;
	  }
	 else {
		
		 return true;
	 }
	  
  }
  function savechanges(){
	  update();
	   
	  if(validate()){
		 //username, first_name, last_name, contact, location, interests, summary, course, courseyear, year, stream, university, grade
		 
		 var xmlhttp=new XMLHttpRequest();
		    xmlhttp.onreadystatechange=function() {
		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		      	
		if (xmlhttp.responseText.indexOf("OK")!=-1){
			document.getElementById("save").style.display="none";
			document.getElementById("error").style.display="none";
			document.getElementById("message").innerHTML="";
		}
		else {
			document.getElementById("error").style.display="";
			document.getElementById("message").innerHTML="There was an error saving changes";
		}


				}
				}
		    xmlhttp.open("POST","UpdateCandidateResume",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xmlhttp.send("first_name="+f_name+"&last_name="+l_name+"&contact="+contact+"&location="+locat
		    		+"&interests="+interests+"&summary="+summary+"&course="+course+"&courseyear="+courseyear+ "&stream="+stream+"&university="+college+"&grade="+grade);
		 
	  } 
	  else {}
  }
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <jsp:include page="candidate-sidebar.jsp"></jsp:include>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        My Resume
        <small>View/Edit</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">My Resume</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content col-lg-6">
      
          <div class="box box-warning ">
              <div class="box-header with-border">
              <h3 class="box-title">Personal Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form">
                <!-- text input -->
                <div class="form-group">
                  <label>First Name</label>
                  <input type="text" id="f_name" class="form-control" onkeyup="update()" placeholder="First Name" 
                  value="<% out.print(user_profile.getFirst_name());  %>"/>
                </div>
                <div class="form-group">
                  <label>Last Name</label>
                  <input type="text" id="l_name"class="form-control" onkeyup="update()" placeholder="Last Name"
                  value="<% out.print(user_profile.getLast_name());  %>" />
                </div>
                <div class="form-group">
                  <label>Contact Number</label>
                  <input type="text" id="contact" class="form-control" onkeyup="update()" placeholder="Contact Number"
                   value="<% out.print(user_profile.getContact());  %>" >
                </div>
                 <div class="form-group">
                  <label>Current Location</label>
                  <input type="text" id="location" class="form-control" onkeyup="update()" placeholder="Your Current Location of Residence"
                   value="<% out.print(user_profile.getLocation());  %>">
                </div>
                                <!-- textarea -->
                <div class="form-group">
                  <label>Interests</label>
                  <textarea class="form-control" id="interests" rows="3" onkeyup="update()" placeholder="Enter Your interests"><% out.print(user_profile.getInterests());  %></textarea>
                </div>
                <div class="form-group">
                  <label>Profile Summary</label>
                  <textarea class="form-control" id="summary" rows="3" onkeyup="update()" placeholder="Give us a description about you in brief"><% out.print(user_profile.getSummary());  %></textarea>
                </div>
<!-- Select multiple-->
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          

    </section>
    
    <section class="content col-lg-6">
      
          <div class="box box-success ">
              <div class="box-header with-border">
              <h3 class="box-title">Academic Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form">
                <!-- text input -->
                <div class="form-group">
                  <label>Course Currently Enrolled in</label>
                  <select class="form-control" onchange="update()" id="course">
                  	<%
                  	String option=user_profile.getCourse();
                  	String value=user_profile.getCourse();
                  	 if (option.equals(""))   {	option="Select your option";value="";}
                  	 %>
                  	 <option value="<%out.print(value);%>" <%	 if (!value.equals(""))out.print("selected=\"true\" disabled=\"disabled\" ");%>><%out.print(option);%></option>
                    <option value="B.Tech">B.Tech</option>
                    <option value="M.Tech">M.Tech</option>
                    <option value="B.Sc">B.Sc</option>
                    <option value="M.Sc">M.Sc</option>
                    <option value="Other">Other</option>
                  </select>
                </div>
               
                <div class="form-group">
                  <label>Year in Pursuing Course</label>
                  <select class="form-control" onchange="update()" id="courseyear">
                  	<%
                  	 option=user_profile.getCourseyear();
                  	 value=user_profile.getCourseyear();
                  	 if (option.equals(""))   {	option="Select your option";value="";}
                  	 %>
                  	 <option value="<%out.print(value);%>" <%	 if (!value.equals(""))out.print("selected=\"true\" disabled=\"disabled\" ");%>><%out.print(option);%></option>
                    <option value="First Year">First Year</option>
                    <option value="Second Year">Second Year</option>
                    <option value="Third Year">Third Year</option>
                    <option value="Fourth Year">Fourth Year</option>
                    <option value="Fifth Year">Fifth Year</option>
                    <option value="Completed">Completed</option>
                  </select>
                </div>
                
                <div class="form-group">
                  <label>Field Of Study</label>
                  <select class="form-control" onchange="update()" id="stream">
                  	<%
                  	 option=user_profile.getStream();
                  	 value=user_profile.getStream();
                  	 if (option.equals(""))   {	option="Select Stream";value="";}
                  	 %>
                  	 <option value="<%out.print(value);%>" <%	 if (!value.equals(""))out.print("selected=\"true\" disabled=\"disabled\" ");%>><%out.print(option);%></option>
                    <option value="Computer Science">Computer Science</option>
                    <option value="Information Technology">Information Technology</option>
                    <option value="Electronics">Electronics</option>
                    <option value="Other">Other</option>
                    
                  </select>
                </div>
                
                
                <div class="form-group">
                  <label>University/College Name</label>
                  <input type="text" id="college" class="form-control" onkeyup="update()" placeholder="Name of University/College currently enrolled in"
                   value="<% out.print(user_profile.getUniversity());  %>">
                </div>
                <div class="form-group">
                  <label>CGPA/Percentage</label>
                  <input type="text" id="grade" class="form-control" onkeyup="update()" placeholder="Grades in current Course"
                   value="<% out.print(user_profile.getGrade());  %>">
                </div>
                <div class="alert alert-danger alert-dismissible" style="display:none" id="error">
                <i class="icon fa fa-ban"></i> 
              <span id="message"></span> 
              </div>
                <div>
                <button type="button" id="save" class="btn btn-block btn-success"  style="display:none" onclick="savechanges()" >Save Changes</button>
                </div>
				
                <!-- textarea -->
                


              </form>
            </div>
            <!-- /.box-body -->
          </div>
          

    </section>
    
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
   <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  
</div>
<jsp:include page="candidate-footer.jsp" />
<!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<!-- AdminLTE for demo purposes -->
<script>
var f_name,l_name,contact,interests,summary,course,courseyear,stream,college,grade,locat;
update();
document.getElementById("save").style.display="none";//hiding save button
</script>
</body>
</html>
