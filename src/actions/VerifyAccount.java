package actions;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.Query;
import email.ZedaEmail;
import exceptions.VerificationFailedException;
import mongo.MongoQuery;
import utilities.Config;

/**
 * Servlet implementation class VerifyAccount
 */
@WebServlet("/VerifyAccount")
public class VerifyAccount extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VerifyAccount() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		VerificationFailedException verification_error =new VerificationFailedException("username");
		 
			try {
				if(Query.verify(request.getParameter("username"), request.getParameter("vercode"))){
					String MessageText="Your account has been verified";
					MongoQuery.newUser(request.getParameter("username"));
				    ZedaEmail.sendEmail(request.getParameter("username"), "Account-IITB Recruitement Portal", MessageText);
			
					response.sendRedirect("login");
					
				}
				else{
					
					throw verification_error;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				response.sendRedirect("VerificationError.jsp");
			} catch (VerificationFailedException e) {
				// TODO Auto-generated catch block
				response.sendRedirect("VerificationError.jsp");
			};
		 
		 

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
