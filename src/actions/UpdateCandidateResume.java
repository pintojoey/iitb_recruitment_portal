package actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.jasper.tagplugins.jstl.core.Out;

import useraccounts.Candidate_Resume;

/**
 * Servlet implementation class UpdateCandidateResume
 */
@WebServlet("/UpdateCandidateResume")
public class UpdateCandidateResume extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateCandidateResume() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			HttpSession session = request.getSession(true);
			Candidate_Resume update=new Candidate_Resume();
			update.setUsername(session.getAttribute("username").toString());
			update.setFirst_name(request.getParameter("first_name"));
			update.setLast_name(request.getParameter("last_name"));
			update.setContact(request.getParameter("contact"));
			update.setLocation(request.getParameter("location"));
			update.setInterests(request.getParameter("interests"));
			update.setSummary(request.getParameter("summary"));
			update.setCourse(request.getParameter("course"));
			update.setCourseyear(request.getParameter("courseyear"));
			update.setStream( request.getParameter("stream"));
			update.setUniversity(request.getParameter("university"));
			update.setGrade(request.getParameter("grade"));
			database.Query.updateCandidateResume(update);
			PrintWriter out = response.getWriter();
		      out.println("OK");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			PrintWriter out = response.getWriter();
		      out.println("Error"); 
		}
	}

}
