package actions;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.Query;
import internships.TechnicalApplication;

/**
 * Servlet implementation class ProcessApplication
 */
@WebServlet("/ProcessApplication")
public class ProcessApplication extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProcessApplication() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		if(request.getParameter("round").equals("2"))
		{
			TechnicalApplication application=new TechnicalApplication();
			application.setUsername(request.getParameter("applicant"));
			application.setIntersnhip_id(Integer.parseInt(request.getParameter("internship_id")));
			application.setAlgo_easy(Integer.parseInt(request.getParameter("algo_easy")));
			application.setAlgo_medium(Integer.parseInt(request.getParameter("algo_medium")));
			application.setAlgo_hard(Integer.parseInt(request.getParameter("algo_hard")));
			application.setDs_easy(Integer.parseInt(request.getParameter("ds_easy")));
			application.setDs_medium(Integer.parseInt(request.getParameter("ds_medium")));
			application.setDs_hard(Integer.parseInt(request.getParameter("ds_hard")));
			application.setJava_basics(Integer.parseInt(request.getParameter("java_basics")));
			application.setJava_web(Integer.parseInt(request.getParameter("java_web")));
			application.setJava_oop(Integer.parseInt(request.getParameter("java_oop")));
			application.setDroid_basics(Integer.parseInt(request.getParameter("droid_basics")));
			application.setDroid_projects(Integer.parseInt(request.getParameter("droid_projects")));
			application.setUnderstanding(Integer.parseInt(request.getParameter("understanding")));
			application.setReasoning(Integer.parseInt(request.getParameter("reasoning")));
			application.setCommunication(Integer.parseInt(request.getParameter("communication")));
			application.setRound2_score(Integer.parseInt(request.getParameter("score")));
			Query.updateTechnicalScreeningScore(application);
		}
		if(request.getParameter("action").equals("hold")){
			Query.hold_application(Integer.parseInt(request.getParameter("round")), request.getParameter("applicant"), Integer.parseInt(request.getParameter("internship_id")),request.getParameter("comment") );
		}
		if(request.getParameter("action").equals("shortlist")){
			Query.shortlist_application(Integer.parseInt(request.getParameter("round")), request.getParameter("applicant"), Integer.parseInt(request.getParameter("internship_id")),request.getParameter("comment") );
		}
		if(request.getParameter("action").equals("waitlist")){
			System.out.println(request.getParameter("round"));
			Query.waitlist_application(Integer.parseInt(request.getParameter("round")), request.getParameter("applicant"), Integer.parseInt(request.getParameter("internship_id")),request.getParameter("comment") );
		}
	}

}
