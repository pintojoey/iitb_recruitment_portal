package actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.Query;
import email.ZedaEmail;
import exceptions.DuplicateKeyException;
import utilities.Config;

/**
 * Servlet implementation class SignUp
 */
@WebServlet("/SignUp")
public class SignUp extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUp() {
        super();
        
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String vercode=utilities.RandomString.getComplexAlphaNumericString(12);
			if(Query.register(request.getParameter("email"), request.getParameter("password"), request.getParameter("type"),request.getParameter("name"),vercode))
			{
			
				response.setContentType("text/plain");  // Set content type of the response so that jQuery knows what it can expect.
			    response.setCharacterEncoding("UTF-8"); // You want world domination, huh?
			    response.getWriter().write("Login"); 
			    
			    String MessageText="<html><body><a href='"+Config.getUrl()+"/VerifyAccount?username="+request.getParameter("email")+"&vercode="+vercode+"'> Click here to verify Registration </a></body></html>";
			    String receiver=ZedaEmail.getAdmin_email();
			    if(request.getParameter("type").equals("candidate"))receiver+=","+request.getParameter("email");
			    ZedaEmail.sendEmail(receiver, request.getParameter("name")+"-Verification Code -IITB Recruitement Portal", MessageText);
			}
		} catch (DuplicateKeyException e) {
			// TODO Auto-generated catch block

			response.setContentType("text/plain");  // Set content type of the response so that jQuery knows what it can expect.
		    response.setCharacterEncoding("UTF-8"); // You want world domination, huh?
		    response.getWriter().write("Exists"); 
			 
			
		}
	}

}
