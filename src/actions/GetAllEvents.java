package actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import mongo.Event;
import mongo.MongoQuery;

/**
 * Servlet implementation class GetEvents
 */
@WebServlet("/GetAllEvents")
public class GetAllEvents extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetAllEvents() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ArrayList<Event> event_list=MongoQuery.getAllEvents();
		Iterator<Event> event_iterator=event_list.iterator();
		JSONArray event_array=new JSONArray();
		while(event_iterator.hasNext()){
			Event current=event_iterator.next();
			JSONObject event_config=new JSONObject();
			try {
				event_config.put("id", current.getId());
				event_config.put("title", current.getTitle());
				event_config.put("start", new SimpleDateFormat("yyyy-MM-dd").format(current.getStart())+"T"+new SimpleDateFormat("HH:mm:ss").format(current.getStart()));
				event_config.put("end", new SimpleDateFormat("yyyy-MM-dd").format(current.getEnd())+"T"+new SimpleDateFormat("HH:mm:ss").format(current.getEnd()));
				event_config.put("allDay", current.isAllday());
				event_config.put("url", current.getUrl());
				event_config.put("editable", true);
				event_config.put("startEditable",true);
				event_config.put("durationEditable", true);
				event_config.put("overlap", true);
				String red="#f56954",blue="#0073b7",yellow="#f39c12",aqua="#00c0ef",green="green";
				if(current.getType().equals("Registration"))event_config.put("color", blue);
				event_config.put("color", green);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			event_array.put(event_config);
		}
		 PrintWriter out=response.getWriter();
	        out.print(event_array.toString());
	}    

}
