package actions;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mongo.Event;
import mongo.MongoQuery;
import mongo.Notification;
import utilities.FormatDate;
import utilities.StringUtils;

/**
 * Servlet implementation class SaveEvent
 */
@WebServlet("/SaveEvent")
public class SaveEvent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaveEvent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			
		Event event=new Event();
		ArrayList<String> candidates=new ArrayList<String>();
		candidates.add(request.getParameter("candidate").toString());
		event.setCandidate_actors(candidates);
		event.setAllday(true);
		ArrayList<String> recruiters=new ArrayList<String>();
		recruiters.add(request.getParameter("id").toString());
		System.out.println("added"+request.getParameter("id").toString());
		event.setRecruiter_actors(recruiters);
		event.setTitle(request.getParameter("name"));
		event.setStart(request.getParameter("start")==null?null:FormatDate.parse(request.getParameter("start")));
		event.setEnd(request.getAttribute("close")==null?null:FormatDate.parse(request.getParameter("close")));
		event.setType(request.getParameter("type"));
		event.setDetail(request.getParameter("detail"));
		event.setUrl(request.getParameter("url")==null?"#":request.getParameter("detail"));
		mongo.MongoQuery.addEvent(event);
		ArrayList<String> users=new ArrayList<String>();
		users.addAll(event.getCandidate_actors());
		users.addAll(event.getRecruiter_actors());
		Notification notif=new Notification();
		notif.setType(event.getType());
		notif.setTitle(request.getParameter("name"));
		notif.setMessage(event.getType()+" scheduled for "+new SimpleDateFormat("EEE dd MMM yyyy").format(event.getStart()));
		notif.setTimestamp(new Date());
		MongoQuery.notify(users, notif);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	
	}

}
