/**
 * 
 */
package email;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import utilities.File;

/**
 * @author Joey
 * Email Settings defined for gmail
 *
 */
public class ZedaEmail {
	public static String admin_email;
	private static String admin_password,admin_name;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new ZedaEmail();
		sendEmail("pintojoey@gmail.com,2013KUCP1021@iiitkota.ac.in","test","body");

	}
	public static String getAdmin_email(){
		if (ZedaEmail.admin_name==null)new ZedaEmail();
		return admin_email;
	}
	public ZedaEmail(){

		try{
			JSONArray config = new JSONArray(File.readFile("config.json"));
			JSONObject jsonObject = config.getJSONObject(0);

			admin_email = (String) jsonObject.get("admin_email");
			admin_password = (String) jsonObject.get("admin_password");
			admin_name = (String) jsonObject.get("admin_name");

		}
		catch(JSONException e){
			System.out.println("Incorrect Configuration file syntax");
		}

	}
	public static boolean sendEmail(String receiver,String Subject,String MessageText) {
		// Recipient's email ID needs to be mentioned.
		if (ZedaEmail.admin_name==null)new ZedaEmail();
		String to = receiver;//change accordingly

		// Sender's email ID needs to be mentioned
		String from = admin_name;//change accordingly
		final String username = admin_email;//change accordingly
		final String password = admin_password;//change accordingly

		URL url;
		try {
			url = new URL("http://www.zedacross.com/iitb_email.php");

			Map<String,Object> params = new LinkedHashMap<>();
			params.put("senderEmail", admin_email);
			params.put("senderName", from);
			params.put("receiverEmail", receiver);
			params.put("contactSubject", Subject);
			params.put("contactMessage", MessageText);

			StringBuilder postData = new StringBuilder();
			for (Map.Entry<String,Object> param : params.entrySet()) {
				if (postData.length() != 0) postData.append('&');
				postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
				postData.append('=');
				postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
			} 
			byte[] postDataBytes = postData.toString().getBytes("UTF-8");

			HttpURLConnection conn;

			conn = (HttpURLConnection)url.openConnection();

			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
			conn.setDoOutput(true);
			conn.getOutputStream().write(postDataBytes);

			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String format="";
			String l;
			while ((l=in.readLine())!=null) {
				format+=l;
			}
   System.out.println(format);
			System.out.println("Sent message successfully....");



			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;

	}
}
