package internships;

import java.util.Date;

public class Application {
private String internship_name,applicant_name,applicant_education,applicant_college,
applicant_contact,applicant_email,round1_comment,round2_panelist,round2_comment,round3_panelist,round3_comment;
private Date round2_date,round3_date;
private int round2_score=0,round3_score=0,internship_id;
/**
 * @return the internship_name
 */
public String getInternship_name() {
	return internship_name;
}
/**
 * @param internship_name the internship_name to set
 */
public void setInternship_name(String internship_name) {
	this.internship_name = internship_name;
}
/**
 * @return the applicant_name
 */
public String getApplicant_name() {
	return applicant_name;
}
/**
 * @param applicant_name the applicant_name to set
 */
public void setApplicant_name(String applicant_name) {
	this.applicant_name = applicant_name;
}
/**
 * @return the applicant_education
 */
public String getApplicant_education() {
	return applicant_education;
}
/**
 * @param applicant_education the applicant_education to set
 */
public void setApplicant_education(String applicant_education) {
	this.applicant_education = applicant_education;
}
/**
 * @return the applicant_college
 */
public String getApplicant_college() {
	return applicant_college;
}
/**
 * @param applicant_college the applicant_college to set
 */
public void setApplicant_college(String applicant_college) {
	this.applicant_college = applicant_college;
}
/**
 * @return the applicant_contact
 */
public String getApplicant_contact() {
	return applicant_contact;
}
/**
 * @param applicant_contact the applicant_contact to set
 */
public void setApplicant_contact(String applicant_contact) {
	this.applicant_contact = applicant_contact;
}
/**
 * @return the applicant_email
 */
public String getApplicant_email() {
	return applicant_email;
}
/**
 * @param applicant_email the applicant_email to set
 */
public void setApplicant_email(String applicant_email) {
	this.applicant_email = applicant_email;
}
/**
 * @return the round1_comment
 */
public String getRound1_comment() {
	return round1_comment;
}
/**
 * @param round1_comment the round1_comment to set
 */
public void setRound1_comment(String round1_comment) {
	this.round1_comment = round1_comment;
}
/**
 * @return the round2_panelist
 */
public String getRound2_panelist() {
	return round2_panelist;
}
/**
 * @param round2_panelist the round2_panelist to set
 */
public void setRound2_panelist(String round2_panelist) {
	this.round2_panelist = round2_panelist;
}
/**
 * @return the round2_comment
 */
public String getRound2_comment() {
	return round2_comment;
}
/**
 * @param round2_comment the round2_comment to set
 */
public void setRound2_comment(String round2_comment) {
	this.round2_comment = round2_comment;
}
/**
 * @return the round3_panelist
 */
public String getRound3_panelist() {
	return round3_panelist;
}
/**
 * @param round3_panelist the round3_panelist to set
 */
public void setRound3_panelist(String round3_panelist) {
	this.round3_panelist = round3_panelist;
}
/**
 * @return the round3_comment
 */
public String getRound3_comment() {
	return round3_comment;
}
/**
 * @param round3_comment the round3_comment to set
 */
public void setRound3_comment(String round3_comment) {
	this.round3_comment = round3_comment;
}
/**
 * @return the round2_date
 */
public Date getRound2_date() {
	return round2_date;
}
/**
 * @param round2_date the round2_date to set
 */
public void setRound2_date(Date round2_date) {
	this.round2_date = round2_date;
}
/**
 * @return the round2_score
 */
public int getRound2_score() {
	return round2_score;
}
/**
 * @param round2_score the round2_score to set
 */
public void setRound2_score(int round2_score) {
	this.round2_score = round2_score;
}
/**
 * @return the round3_score
 */
public int getRound3_score() {
	return round3_score;
}
/**
 * @param round3_score the round3_score to set
 */
public void setRound3_score(int round3_score) {
	this.round3_score = round3_score;
}
/**
 * @return the round3_date
 */
public Date getRound3_date() {
	return round3_date;
}
/**
 * @param round3_date the round3_date to set
 */
public void setRound3_date(Date round3_date) {
	this.round3_date = round3_date;
}

/**
 * @return the internship_id
 */
public int getInternship_id() {
	return internship_id;
}
/**
 * @param internship_id the internship_id to set
 */
public void setInternship_id(int internship_id) {
	this.internship_id = internship_id;
}


}
