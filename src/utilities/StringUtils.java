package utilities;

import java.util.ArrayList;


public class StringUtils {
	public static String StringArrayListToString(ArrayList<String> list) {
	    StringBuffer sb = new StringBuffer();
	    for (String Object : list){
	        sb.append(Object).append(",");
	    } 
	    sb.deleteCharAt(sb.lastIndexOf(","));
	    return sb.toString();
	}
}
