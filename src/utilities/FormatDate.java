package utilities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class FormatDate {
	  public static Date parse( String input ) throws java.text.ParseException {

	        //NOTE: SimpleDateFormat uses GMT[-+]hh:mm for the TZ which breaks
	        //things a bit.  Before we go on we have to repair this.
	        SimpleDateFormat df = new SimpleDateFormat( "EEE MMM dd yyyy HH:mm:ss" );
	       
	        input=input.substring(0, input.length()-9);
	        return df.parse( input );
	        
	    }
	  public static Date parseIST( String input ) throws java.text.ParseException {

	        //NOTE: SimpleDateFormat uses GMT[-+]hh:mm for the TZ which breaks
	        //things a bit.  Before we go on we have to repair this.
	        SimpleDateFormat df = new SimpleDateFormat( "EEE MMM dd HH:mm:ss 'IST' yyyy" );
	       
	        
	        return df.parse( input );
	        
	    }
	  /*public static String relativeDate(Date date) {
			// function to compare date to present and print period elapsed 
			Date now=new Date();
			long difference=now.getTime()-date.getTime();
			if(difference<1)
			{
				return new SimpleDateFormat("HH:mm:ss MM/dd/yyyy").format(date).toString();
			}
			final int divSet[]={1000,60,60,24,7,4,12,Integer.MAX_VALUE};
			final String timeSet[]={"millisecond","second","minute","hour","day","week","month","year"};
			int info=0,at=0;
			for(int i=0;i<divSet.length;i++)
			{
				if(difference%divSet[i]!=0)
				{
					info=(int) (difference%divSet[i]);
					at=i;
				}
				difference/=divSet[i];
			}
			if(info!=1)
			{
				if(at==timeSet.length-1)
					return new SimpleDateFormat("HH:mm:ss MM/dd/yyyy").format(date).toString();			
				return (info+" "+timeSet[at]+"s ago");
			}
			else
			{
				switch(at)
				{
				case 0:
				case 1:
				case 2:
				case 3:
					return ("an "+timeSet[at]+" ago");
				case 4:
					return ("yesterday");
				case 5:
				case 6:
				case 7:
					return ("last "+timeSet[at]);
				}
			}
			return new SimpleDateFormat("HH:mm:ss MM/dd/yyyy").format(date).toString();
			
		}*/
	  
	  public static String relativeDate(Date date){
		Date now=new Date();
		if(date.before(now)){
		int days_passed=(int) TimeUnit.MILLISECONDS.toDays(now.getTime() - date.getTime());
		int hours_passed=(int) TimeUnit.MILLISECONDS.toHours(now.getTime() - date.getTime());
		int minutes_passed=(int) TimeUnit.MILLISECONDS.toMinutes(now.getTime() - date.getTime());
		int seconds_passed=(int) TimeUnit.MILLISECONDS.toSeconds(now.getTime() - date.getTime());
		if(days_passed==1)return "1 day ago";
		else if(days_passed>1)return days_passed+" days ago";
		else{
			
			if(hours_passed==1)return "an hour ago";
			else if(hours_passed>1)return hours_passed+" hours ago";
			else{
				if(minutes_passed==1)return "an minute ago";
				else if(minutes_passed>1)return minutes_passed+" minutes ago";
				else{
					if(seconds_passed<=10)return "just now";
					else return seconds_passed +" seconds ago";
				}
			}
		}
		 
		}
		else
		{
			return new SimpleDateFormat("HH:mm:ss MM/dd/yyyy").format(date).toString();
		}
	  }
}
