package database;


import java.io.BufferedReader;

import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.DriverManager;

import java.sql.SQLException;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import actions.SignUp;
import utilities.File;






public class Connection {
static String database,host,password,username;
public static com.mysql.jdbc.Connection con;
public Connection() {
	try{
		JSONArray config = new JSONArray(File.readFile("config.json"));
	 JSONObject jsonObject = config.getJSONObject(0);

   database = (String) jsonObject.get("database");
   host = (String) jsonObject.get("host");
   username = (String) jsonObject.get("username");
   password = (String) jsonObject.get("password");
    
    Class.forName("com.mysql.jdbc.Driver"); 
    
	 con = (com.mysql.jdbc.Connection) DriverManager.getConnection("jdbc:mysql://"+host+"/"+database,username,password); 
	}
	catch(JSONException e){
		System.out.println("Incorrect Configuration file syntax");
	}
	catch(SQLException e){
		System.out.println("Could not connect to database");
	}
	catch(ClassNotFoundException e){
		System.out.println("Could not find JDBC driver");
	}
}
public static void close () throws SQLException{
	con.close();
}

public static void main(String []args){
	new Connection();
	
}
}
